package b2t

import grails.converters.JSON
import grails.validation.ValidationException
import grails.plugins.rest.client.RestBuilder
import groovy.json.JsonOutput
import groovy.json.JsonSlurper

import static org.springframework.http.HttpStatus.*

class ArticleController {

    ArticleService articleService
    MarketingTextService marketingTextService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def generateAttributeText = {
        def articleId = params.articleId
        def article = Article.findByArticle_id(articleId)

        def result = callAzure()

        marketingTextService.generateMarketingTextForJson(result.body)

        article.attribute_text = marketingTextService.getAttributes().toString()
        articleService.save(article)
        redirect article
    }

    def generateMarketingText = {
        def articleId = params.articleId
        def article = Article.findByArticle_id(articleId)

        def result = callAzure()

        def marketingText =  marketingTextService.generateMarketingTextForJson(result.body)
        article.marketing_text = marketingText
        articleService.save(article)

        def resultObject = new JsonSlurper().parseText(result.body)
        //render resultObject as JSON
        redirect article
    }

    def generateSmallImage = {

        def articleId = params.articleId
        def article = Article.findByArticle_id(articleId)

        if ("999999".equals(articleId)){
            article.useImage2 = "true"
            articleService.save(article)
        }
        else {
            def result = detect()

            def objects = new JsonSlurper().parseText(result.body).objects
            if (objects != null) {
                def rect = new JsonSlurper().parseText(result.body).objects.rectangle
                if (rect) {
                    article.top = rect.y.get(0)
                    article.bottom = rect.y.get(0) + rect.h.get(0)
                    article.left = rect.x.get(0)
                    article.right = rect.x.get(0) + rect.w.get(0)
                    articleService.save(article)
                }
            }
        }
        //render resultObject as JSON
        //redirect article, params: [rect: resultObject]
        //style="position:absolute;clip:rect(${this.rect.y}px,${this.rect.y+rect.h}px,${this.rect.x+rect.w}px,${this.rect.x}px)"/>
        redirect article
    }

    def callAzure() {
        RestBuilder rest = new RestBuilder()
        String articleId = params.articleId
        String url = "https://westeurope.api.cognitive.microsoft.com/vision/v2.0/analyze?visualFeatures=Brands,Categories,Color,Description,ImageType,Objects,Tags"
        def image = new File(System.properties["user.dir"] + "/grails-app/assets/images/article/woman_sweater/" + articleId + "_free_standing.png").bytes
        return rest.post(url) {
            header 'Ocp-Apim-Subscription-Key', '0f997568fa7f43b387d18a4140461f7b'
            contentType "multipart/form-data"
            accept("application/json")
            setProperty "file", image
        }
    }

    def detect() {
        RestBuilder rest = new RestBuilder()
        String articleId = params.articleId
        String url = "https://westeurope.api.cognitive.microsoft.com/vision/v2.0/detect"
        def image = new File(System.properties["user.dir"] + "/grails-app/assets/images/article/woman_sweater/" + articleId + "_free_standing.png").bytes
        return rest.post(url) {
            header 'Ocp-Apim-Subscription-Key', '0f997568fa7f43b387d18a4140461f7b'
            contentType "multipart/form-data"
            accept("application/json")
            setProperty "file", image
        }
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond articleService.list(params), model:[articleCount: articleService.count()]
    }

    def show(Long id) {
        respond articleService.get(id)
    }

    def shopdetail(Long id) {
        respond articleService.get(id)
    }

    def shoplist(Long id) {
        respond articleService.get(id)
    }

    def create() {
        respond new Article(params)
    }

    def save(Article article) {
        if (article == null) {
            notFound()
            return
        }

        try {
            articleService.save(article)
        } catch (ValidationException e) {
            respond article.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'article.label', default: 'Article'), article.id])
                redirect article
            }
            '*' { respond article, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond articleService.get(id)
    }

    def update(Article article) {
        if (article == null) {
            notFound()
            return
        }

        try {
            articleService.save(article)
        } catch (ValidationException e) {
            respond article.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'article.label', default: 'Article'), article.id])
                redirect article
            }
            '*'{ respond article, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        articleService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'article.label', default: 'Article'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'article.label', default: 'Article'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
