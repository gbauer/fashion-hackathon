package b2t

import com.fasterxml.jackson.databind.ObjectMapper
import grails.gorm.transactions.Transactional


@Transactional
class MarketingTextService {

    @SuppressWarnings("serial")
    private HashMap<String, String> attributes = new HashMap<String, String>() {

        {
            put("OBJECT", null);
            put("COLOR", null);
            put("BRAND", null);
        }
    }

    String generateMarketingTextForJson(String jsonString){
        def retVal

        attributes = new HashMap<String, String>() {

		{
			put("OBJECT", null);
			put("COLOR", null);
			put("BRAND", null);
		}
	    };

        extractAttributesFromJSONResponse(jsonString);
        retVal = getMarketingText(6)
     
        return retVal
    }
    
    @SuppressWarnings("serial")
    private List<String> sentenceTemplates = new ArrayList<String>() {

        {
            add("In the picture you can see a beautiful OBJECT.");
            add("In the picture you can see a beautiful product in the color COLOR.");
            add("In the picture you can see a beautiful product from BRAND.");
            add("In the picture you can see a beautiful COLOR OBJECT.");
            add("In the picture you can see a beautiful OBJECT from BRAND.");
            add("In the picture you can see a beautiful product in the color COLOR from BRAND.");
            add("In the picture you can see a beautiful COLOR OBJECT from BRAND.");
            add("Here we have a COLOR OBJECT.");
            add("This is a really cool OBJECT.");
            add("The OBJECT is one of our most buyed product.");
            add("The most of our customers buyed this OBJECT in the color COLOR");
            add("This is our new COLOR OBJECT of the brand BRAND.");
            add("Here you can see our amazing new COLOR OBJECT of the brand BRAND.");
            add("This is our brandnew COLOR OBJECT of the brand BRAND. As long as stocks last.");
            add("Watch out. The latest collection of the brand BRAND includes this cool COLOR OBJECT");
            add("New in stock. The latest collection of the brand BRAND includes this amazing COLOR OBJECT");
            add("Access during our BRAND special sales week. Today our COLOR OBJECT. As long as stocks last.");
            add("This is the offer of the day from BRAND.");
            add("The color is the original BRAND color.");
            add("With BRAND products you are really cool.");
        }
    };

    // Return all found attributes as Mapss
    public Map<String, String> getAttributes() {
        final Map<String, String> m = new HashMap<String, String>();
        attributes.each { k, v ->
            if (v != null) {
                m.put(k, v);
            }
        }
        return m;
    }

    public String getMarketingText() {
        return getMarketingText(10000);
    }

    // generate Marketing Text
    public String getMarketingText(int maxSentenceCount) {
        // fill sentence templates
        for (int i = 0; i < sentenceTemplates.size(); i++) {
            for (String k : attributes.keySet()) {
                final String v = attributes.get(k);
                if (v != null) {
                    sentenceTemplates.set(i, sentenceTemplates.get(i).replaceAll(k, v));
                }
            }
        }

        // pick only filled sentences
        final List<String> filledSentences = new ArrayList<String>();
        for (String s : sentenceTemplates) {
            boolean isComplete = true;
            for (String k : attributes.keySet()) {
                final String v = attributes.get(k);
                if (s.contains(k)) {
                    isComplete = false;
                }
            }
            if (isComplete) {
                filledSentences.add(s);
            }
        }

        // randomize output data and set sentence count
        final List<String> marketingSentences = new ArrayList<String>();
        Collections.shuffle(filledSentences);
        for (int i = 0; i < filledSentences.size() && i < maxSentenceCount; i++) {
            marketingSentences.add(filledSentences.get(i));
        }

        return String.join(" ", marketingSentences);
    }

    // Extract data from JSON String
    @SuppressWarnings("unchecked")
    private void extractAttributesFromJSONResponse(final String jsonObjAsString) {
        HashMap<String, Object> result = null;
        try {
            result = new ObjectMapper().readValue(jsonObjAsString, HashMap.class);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        if (result != null) {
            List<Map<String, String>> b = (List<Map<String, String>>) result.get("brands");
			if (b != null && b.size() != 0) {
				Map<String, String> c = (Map<String, String>) b.get(0);
				attributes.put("BRAND", (String) c.get("name"));
			}

            Map<String, Object> m = (Map<String, Object>) result.get("color");
            if (m != null) {
                if (m.get("dominantColorForeground") != null) {
                    attributes.put("COLOR", (String) m.get("dominantColorForeground"));
                }
            }

            List<Object> o = (List<Object>) result.get("objects");
            if (o != null && o.size() != 0) {
                Map<String, Object> e = (Map<String, Object>) o.get(0);
                if (e != null) {
                    attributes.put("OBJECT", (String) e.get("object"));
                }
            }
        }
    }
}
