package b2t

class Article {

    String article_id
    String product_id
    String product_group

    String passform
    String farbfamilie
    String produkttyp
    String muster
    String groesse
    String material
    String dehnbarkeit
    String bundverarbeitung
    String transparenz
    String pflegehinweise
    String zielgruppe
    String laenge
    String gesamtlaenge_ca
    String materialhinweis
    String gefuettert
    String materialeigenschaften
    String saum
    String farbbezeichnung
    String leibhoehe
    String stil

    String attribute_text
    String marketing_text
    String top
    String bottom
    String left
    String right

    String useImage2


    static constraints = {
        article_id nullable: false, size: 6..10
        attribute_text type: "text", widget: "textArea"
        marketing_text type: "text", widget: "textArea"
    }

    String toString() {
        "$article_id"
    }
}

