<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'article.label', default: 'Article')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

        <a href="#show-article" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><g:link class="list" action="index">Artikel Übersicht</g:link></li>
                <li><g:link class="create" action="create">Artikel anlegen</g:link></li>
            </ul>
        </div>
        <div id="show-article" class="content scaffold-show" role="main">
            <h1>Artikel anzeigen</h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>

            <ol class="property-list article">
                <li class="fieldcontain">
                    <span id="article_id-label" class="property-label">&nbsp;</span>
                    <div class="property-value" aria-labelledby="article_id-label">
                        <g:if test="${!('999999'.equals(this.article.article_id)) || this.article.useImage2}">
                            <asset:image height="300px" src="article/woman_sweater/${this.article.article_id}_free_standing.png"/>
                        </g:if>
                        <g:else>
                            <asset:image height="300px" src="article/woman_sweater/${this.article.article_id}handy_free_standing.png"/>
                        </g:else>
                        <g:if test="${this.article.top}">
                            <asset:image height="300px;" src="article/woman_sweater/${this.article.article_id}_free_standing.png"
                                         style="position:absolute;clip:rect(${this.article.top}px,${this.article.right}px,${this.article.bottom}px,${this.article.left}px)"/>
                        </g:if>
                        <br>
                        <g:link class="edit" action="generateSmallImage" params="[articleId: this.article.article_id]">Bild optimieren</g:link>
                        <br>
                        <g:link class="edit" action="generateAttributeText" params="[articleId: this.article.article_id]">Attribute generieren</g:link>
                        <br>
                        <g:link class="edit" action="generateMarketingText" params="[articleId: this.article.article_id]">Marketingtexte generieren</g:link>
                        <br>
                        <g:link class="edit" action="shopdetail" params="[articleId: this.article.article_id, id:article.id]">Im Shop anzeigen</g:link>
                    </div>
                </li>
            </ol>

            <g:if test="${false}">
            <div>
                <pre>
                    ${article.attribute_text}
                </pre>
            </div>
            </g:if>

            <f:display bean="article" />

            <g:form resource="${this.article}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.article}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
