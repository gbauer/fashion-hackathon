import os
import shutil

print('Start')

# Setup Vars
files = []

file = open("ids.txt")
input_path = 'C:\\Projects\\fashion-hackathon\\b2t\\grails-app\\assets\\images\\article\\images_free_standing'
output_path = 'C:\\Projects\\fashion-hackathon\\b2t\\grails-app\\assets\\images\\article\\woman_sweater'

# Get Images
for line in file:
    line = line.replace('\n','')
    for r, d, f in os.walk(input_path):
        for file in f:
            if line in file:
                files.append(os.path.join(r, file))

# Store Images
for file in files:
    shutil.copy(file, output_path)

print('Done')
