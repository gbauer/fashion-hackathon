grails.gorm.default.constraints = {
    '*' (nullable: true, blank: true)
}


// Enable pretty print for JSON.
//grails.converters.json.pretty.print = true

// Enable pretty print for XML.
//grails.converters.xml.pretty.print = true

// Enable pretty print for JSON and XML.
grails.converters.default.pretty.print = true